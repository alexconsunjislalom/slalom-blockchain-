import hashlib as hasher
import datetime as date
import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blocks', __name__)

@bp.route('/')
def index():
    """Show all the posts, most recent first."""
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('blocks/index.html', posts=posts)


def get_post(id, check_author=True):
    """Get a post and its author by id.
    Checks that the id exists and optionally that the current user is
    the author.
    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


@bp.route('/open', methods=('GET', 'POST'))
@login_required
def open():
    """Create a new post for the current user."""
    if request.method == 'POST':
        title = request.form['title']
        fulldate = date.datetime.now()
        fulldate = fulldate + date.timedelta(milliseconds=500)
        hash_object = hasher.sha256(title + str(fulldate) + str(g.user['id']))
        hex_dig = hash_object.hexdigest()
        title = hex_dig
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blocks.index'))

    return render_template('blocks/create.html')


# @bp.route('/<int:id>/update', methods=('GET', 'POST'))
# @login_required
# def update(id):
#     """Update a post if the current user is the author."""
#     post = get_post(id)

#     if request.method == 'POST':
#         title = request.form['title']
#         body = request.form['body']
#         error = None

#         if not title:
#             error = 'Title is required.'

#         if error is not None:
#             flash(error)
#         else:
#             db = get_db()
#             db.execute(
#                 'UPDATE post SET title = ?, body = ? WHERE id = ?',
#                 (title, body, id)
#             )
#             db.commit()
#             return redirect(url_for('blog.index'))

#     return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    """Delete a post.
    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('block.index'))

# #define block
# class Block:
#   def __init__(self, index, timestamp, data, previous_hash):
#     self.index = index
#     self.created = timestamp
#     self.timestamp = timestamp
#     self.data = data
#     self.previous_hash = previous_hash
#     self.hash = self.hash_block()
  
#   def hash_block(self):
#     sha = hasher.sha256()
#     sha.update(str(self.index) + str(self.timestamp) + str(self.data) + str(self.previous_hash))
#     return sha.hexdigest()


# # Generate all later blocks in the blockchain
# def next_block(last_block):
#   this_index = last_block.index + 1
#   this_timestamp = date.datetime.now()
#   this_data = "Hey! I'm block " + str(this_index)
#   this_hash = last_block.hash
#   return Block(this_index, this_timestamp, this_data, this_hash)

# #Generate genesis block
# def create_genesis_block():
#   # Manually construct a block with
#   # index zero and arbitrary previous hash
#   return Block(0, date.datetime.now(), "Super Secret Evidence First Block", "0")

# # # Create the blockchain and add the genesis block
# blockchain = []
# blockchain.append(create_genesis_block())
# previous_block = blockchain[0]

# # How many blocks should we add to the chain
# # after the genesis block
# num_of_blocks_to_add = 20

# # Add blocks to the chain
# for i in range(0, num_of_blocks_to_add):
#   block_to_add = next_block(previous_block)
#   blockchain.append(block_to_add)
#   previous_block = block_to_add
#   # Tell everyone about it!
#   print "Block #{} has been added to the blockchain!".format(block_to_add.index)
#   print "Hash: {}\n".format(block_to_add.hash) 

# @bp.route('/')
# def index():
#     db = get_db()
#     blockchain = date.datetime.now()
#     return render_template('blocks/index.html', blockchain = blockchain)

# @bp.route('/open', methods=('GET', 'POST'))
# @login_required
# def open():
#     #Haoli put the socket code here to send the open command

#     # blockchain = blockchain
#     # last_block = blockchain[len(blockchain) - 1]
#     # this_index = last_block.index + 1
#     # this_timestamp = date.datetime.now()
#     # this_data = "Hey! I'm block " + str(this_index)
#     # this_hash = last_block.hash
#     # block_to_add = Block(this_index, this_timestamp, this_data, this_hash)
#     # blockchain.append(block_to_add)
#     date = date.datetime.now()

#     return redirect(url_for('blocks.index'), blockchain = date)
    
@bp.route('/close', methods=('GET', 'POST'))
@login_required
def close():
    #Haoli put the socket code here to send the close command
    return redirect(url_for('blocks.index')) 
